# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import mods.jei.JEI.removeAndHide as rh;

print(">>> loading forestry.zs");

rh(<forestry:can:*>);
rh(<forestry:capsule:*>);
rh(<forestry:refractory:*>);
rh(<forestry:arboretum:*>);
rh(<forestry:farm_crops:*>);
rh(<forestry:farm_mushroom:*>);
rh(<forestry:farm_gourd:*>);
rh(<forestry:farm_nether:*>);
rh(<forestry:farm_ender:*>);
rh(<forestry:peat_bog:*>);
