# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import mods.enderio.AlloySmelter as eio_alloysmelter;
import mods.immersiveengineering.AlloySmelter as ie_alloysmelter;
import mods.immersiveengineering.ArcFurnace as ie_arcfurnace;

print(">>> loading alloys.zs");

# EnderIO Alloy Smelter Recipes
eio_alloysmelter.addRecipe(<basemetals:brass_ingot> * 4, [ <ore:ingotCopper> * 3, <ore:ingotZinc> ]);
eio_alloysmelter.addRecipe(<basemetals:pewter_ingot> * 3, [ <ore:ingotTin>, <ore:ingotCopper>, <ore:ingotLead> ]);
eio_alloysmelter.addRecipe(<basemetals:steel_ingot>, [ <ore:ingotIron>, <ore:dustCoke> ]);
eio_alloysmelter.addRecipe(<tconstruct:ingots:5> * 4, [ <ore:ingotCopper> * 3, <ore:ingotAluminum> ]);
eio_alloysmelter.addRecipe(<plustic:alumiteingot> * 3, [ <ore:ingotAluminum> * 5, <ore:ingotIron> * 2, <ore:obsidian> * 2 ]);
eio_alloysmelter.addRecipe(<modernmetals:galvanizedsteel_ingot> * 3, [ <ore:ingotSteel> * 2, <ore:ingotZinc> ]);
eio_alloysmelter.addRecipe(<modernmetals:stainlesssteel_ingot> * 3, [ <ore:ingotSteel> * 2, <ore:ingotChrome> ]);
eio_alloysmelter.addRecipe(<modernmetals:nichrome_ingot> * 3, [ <ore:ingotNickel> * 2, <ore:ingotChrome> ]);

# Immersive Engineering Alloy Kiln Recipes
ie_alloysmelter.addRecipe(<modernmetals:galvanizedsteel_ingot> * 3, <ore:ingotSteel> * 2, <ore:ingotZinc>, 2000);
ie_alloysmelter.addRecipe(<modernmetals:stainlesssteel_ingot> * 3, <ore:ingotSteel> * 2, <ore:ingotChrome>, 2000);
ie_alloysmelter.addRecipe(<modernmetals:nichrome_ingot> * 3, <ore:ingotNickel> * 2, <ore:ingotChrome>, 2000);
ie_alloysmelter.addRecipe(<tconstruct:ingots:5> * 4, <ore:ingotCopper> * 3, <ore:ingotAluminum>, 2000);
ie_alloysmelter.addRecipe(<tconstruct:ingots:2> * 1, <ore:ingotArdite>, <ore:ingotCobalt>, 2000);

# Immersive Engineering Arc Furnace Recipes
ie_arcfurnace.addRecipe(<basemetals:steel_ingot>, <ore:ingotIron>, <immersiveengineering:material:7>, 200, 512, [ <ore:dustCoal> * 4 ]);
ie_arcfurnace.addRecipe(<modernmetals:galvanizedsteel_ingot> * 3, <ore:ingotZinc>, null, 200, 512, [ <ore:ingotSteel> * 2 ]);
ie_arcfurnace.addRecipe(<modernmetals:stainlesssteel_ingot> * 3, <ore:ingotChrome>, null, 200, 512, [ <ore:ingotSteel> * 2 ]);
ie_arcfurnace.addRecipe(<modernmetals:nichrome_ingot> * 3, <ore:ingotChrome>, null, 200, 512, [ <ore:ingotNickel> * 2 ]);
