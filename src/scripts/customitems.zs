# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

#loader contenttweaker

import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Color;

print(">>> loading customitems.zs");

# Glue Compound
var gluecompound = VanillaFactory.createItem("gluecompound");
gluecompound.register();

# Solid Glue
var solidglue = VanillaFactory.createItem("solidglue");
solidglue.register();
