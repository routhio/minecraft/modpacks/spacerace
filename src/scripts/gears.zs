# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading gears.zs");

# Remove all gear crafting recipes.
var gearsToRemove = [
    <enderio:item_material:9>,
    <enderio:item_material:10>,
    <forestry:gear_bronze>,
    <forestry:gear_copper>,
    <forestry:gear_tin>,
    <moreplates:iron_gear>,
    <moreplates:steel_gear>
] as IItemStack[];

for gear in gearsToRemove {
    rh(gear);
}

var gearsRecipesToRemove = [
    <basemetals:copper_gear>,
    <basemetals:lead_gear>,
    <basemetals:nickel_gear>,
    <basemetals:silver_gear>,
    <basemetals:tin_gear>,
    <basemetals:zinc_gear>,
    <basemetals:brass_gear>,
    <basemetals:bronze_gear>,
    <basemetals:electrum_gear>,
    <basemetals:invar_gear>,
    <basemetals:pewter_gear>,
    <basemetals:steel_gear>,
    <basemetals:adamantine_gear>,
    <basemetals:coldiron_gear>,
    <basemetals:platinum_gear>,
    <basemetals:iron_gear>,
    <basemetals:gold_gear>,
    <modernmetals:aluminum_gear>,
    <modernmetals:chromium_gear>,
    <modernmetals:magnesium_gear>,
    <modernmetals:rutile_gear>,
    <modernmetals:galvanizedsteel_gear>,
    <modernmetals:nichrome_gear>,
    <modernmetals:stainlesssteel_gear>,
    <modernmetals:uranium_gear>,
    <modernmetals:titanium_gear>
] as IItemStack[];

for gear in gearsRecipesToRemove {
    recipes.remove(gear);
}

# Casting recipes for gears that are missing them
mods.tconstruct.Casting.addTableRecipe(<basemetals:bronze_gear>, <tconstruct:cast_custom:4>, <liquid:bronze>, 576);
mods.tconstruct.Casting.addTableRecipe(<basemetals:copper_gear>, <tconstruct:cast_custom:4>, <liquid:copper>, 576);
#mods.tconstruct.Casting.addTableRecipe(<basemetals:steel_gear>, <tconstruct:cast_custom:4>, <liquid:steel>, 576);
mods.tconstruct.Casting.addTableRecipe(<basemetals:tin_gear>, <tconstruct:cast_custom:4>, <liquid:tin>, 576);
mods.tconstruct.Casting.addTableRecipe(<basemetals:iron_gear>, <tconstruct:cast_custom:4>, <liquid:iron>, 576);
mods.tconstruct.Casting.addTableRecipe(<moreplates:redstone_gear>, <tconstruct:cast_custom:4>, <liquid:redstone>, 500);
