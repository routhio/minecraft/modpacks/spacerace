# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading morebees.zs");

# Fix Rocky Comb
mods.forestry.Centrifuge.addRecipe([<forestry:beeswax> % 100,
                                    <minecraft:cobblestone> % 90,
                                    <minecraft:stone> % 50,
                                    (<minecraft:stone> * 3) % 50,
                                    (<minecraft:stone> * 5) % 50],
                                   <morebees:combrock>, 100);
