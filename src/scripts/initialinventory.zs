# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading initialinventory.zs");

mods.initialinventory.InvHandler.addStartingItem(<patchouli:guide_book>.withTag({"patchouli:book": "patchouli:spacerace"}));
mods.initialinventory.InvHandler.addStartingItem(<prefab:item_start_house>);
