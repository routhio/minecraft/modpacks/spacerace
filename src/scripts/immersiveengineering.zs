# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading immersiveengineering.zs");

# Redstone Engineering Block
recipes.remove(<immersiveengineering:metal_decoration0:3>);
recipes.addShaped("Redstone Engineering Block",  <immersiveengineering:metal_decoration0:3> * 2, 
    [[<ore:plateCopper>, <ore:ingotConstantan>, <ore:plateCopper>],
     [<ore:ingotConstantan>, <ore:gearRedstone>, <ore:ingotConstantan>], 
     [<ore:plateCopper>, <ore:ingotConstantan>, <ore:plateCopper>]]);

# Light Engineering Block
recipes.remove(<immersiveengineering:metal_decoration0:4>);
recipes.addShaped(<immersiveengineering:metal_decoration0:4>, 
    [[<basemetals:iron_plate>, <immersiveengineering:material:8>, <basemetals:iron_plate>],
     [<basemetals:copper_ingot>, <basemetals:copper_ingot>, <basemetals:copper_ingot>], 
     [<basemetals:iron_plate>, <immersiveengineering:material:8>, <basemetals:iron_plate>]]);

# Heavy Engineering Block
recipes.remove(<immersiveengineering:metal_decoration0:5>);
recipes.addShapedMirrored(<immersiveengineering:metal_decoration0:5>, 
    [[<basemetals:steel_plate>, <immersiveengineering:material:9>, <basemetals:steel_plate>],
     [<minecraft:piston>, <basemetals:gold_plate>, <minecraft:piston>], 
     [<basemetals:steel_plate>, <immersiveengineering:material:9>, <basemetals:steel_plate>]]);
