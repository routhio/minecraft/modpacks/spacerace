# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading customrecipes.zs");

# Glue Compound
recipes.addShapeless(<contenttweaker:gluecompound>, [<ore:sand>, <ic2:misc_resource:4>]);
recipes.addShapeless(<contenttweaker:gluecompound>*2, [<ore:sand>, <ore:itemRubber>]);
mods.immersiveengineering.AlloySmelter.addRecipe(<contenttweaker:gluecompound>, <ore:sand>, <ic2:misc_resource:4>, 4000);
mods.immersiveengineering.AlloySmelter.addRecipe(<contenttweaker:gluecompound> * 2, <ore:sand>, <ore:itemRubber>, 4000);

# Solid Glue
recipes.addShapeless(<contenttweaker:solidglue>, [<ic2:fluid_cell:0>, <contenttweaker:gluecompound>]);
recipes.addShapeless(<contenttweaker:solidglue>, [<ic2:fluid_cell:0>, <ore:sand>, <ic2:misc_resource:4>]);
recipes.addShapeless(<contenttweaker:solidglue>*2, [<ic2:fluid_cell:0>, <ic2:fluid_cell:0>, <ore:sand>, <ore:itemRubber>]);

# Wood Glue Universal Fluid Cell
furnace.addRecipe(<ic2:fluid_cell>.withTag({Fluid: {FluidName: "woodglue", Amount: 1000}}), <contenttweaker:solidglue>);
