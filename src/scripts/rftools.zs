# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading rftools.zs");

# Machine Frame
recipes.remove(<rftools:machine_frame>);
recipes.addShaped(<rftools:machine_frame>, 
    [[<modernmetals:galvanizedsteel_plate>, <modernmetals:galvanizedsteel_plate>, <modernmetals:galvanizedsteel_plate>],
     [<immersiveengineering:material:27>, <enderio:item_material:1>, <immersiveengineering:material:27>], 
     [<modernmetals:galvanizedsteel_plate>, <moreplates:lapis_lazuli_gear>, <modernmetals:galvanizedsteel_plate>]]);
