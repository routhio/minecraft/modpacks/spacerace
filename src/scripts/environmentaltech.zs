# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading environmentaltech.zs");

# Laser Lens
recipes.remove(<environmentaltech:laser_lens>);
recipes.addShaped(<environmentaltech:laser_lens>, 
    [[<sonarcore:stableglass>, null, <sonarcore:stableglass>],
     [<sonarcore:stableglass>, <sonarcore:stableglass>, <sonarcore:stableglass>],
     [<sonarcore:stableglass>, null, <sonarcore:stableglass>]]);

# Connector
recipes.remove(<environmentaltech:connector>);
recipes.addShaped(<environmentaltech:connector>,
    [[<moreplates:redstone_alloy_plate>, <basemetals:iron_plate>, <moreplates:redstone_alloy_plate>],
     [<basemetals:iron_plate>, <integrateddynamics:part_connector_omni_directional_item>, <basemetals:iron_plate>],
     [<moreplates:redstone_alloy_plate>, <basemetals:iron_plate>, <moreplates:redstone_alloy_plate>]]);

# Litherite Crystal
recipes.remove(<environmentaltech:litherite_crystal>);
recipes.addShaped(<environmentaltech:litherite_crystal>, 
    [[<extraplanets:tier11_items:4>, <vt:flintblock>, <extraplanets:tier11_items:4>],
     [<vt:flintblock>, <enderio:item_material:17>, <vt:flintblock>], 
     [<extraplanets:tier11_items:4>, <vt:flintblock>, <extraplanets:tier11_items:4>]]);

