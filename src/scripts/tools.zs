# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading tools.zs");

# No moreplates hammer
rh(<moreplates:hammer>);

# Immersive Engineering hammer
recipes.remove(<immersiveengineering:tool>);
recipes.addShaped(<immersiveengineering:tool>, 
                  [[<ore:ingotIron>, <ore:ingotIron>, <ore:ingotIron>],
                   [<ore:string>, <ore:stickWood>, <ore:string>], 
                   [null, <ore:stickWood>, null]]);
