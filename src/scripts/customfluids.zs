# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

#loader contenttweaker

import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Fluid;
import mods.contenttweaker.Color;

print(">>> loading customfluids.zs");

# Wood glue

var woodglue = VanillaFactory.createFluid("woodglue", Color.fromHex("e2cdaf"));
woodglue.fillSound = <soundevent:block.note.snare>;
woodglue.luminosity = 8;
woodglue.viscosity = 3000;
woodglue.rarity = "UNCOMMON";
woodglue.register();

# Superglue

var superglue = VanillaFactory.createFluid("superglue", Color.fromHex("dadda9"));
superglue.fillSound = <soundevent:block.note.snare>;
superglue.luminosity = 8;
superglue.viscosity = 5000;
superglue.rarity = "UNCOMMON";
superglue.register();

