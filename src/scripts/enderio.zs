# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading enderio.zs");

# Simple Machine Chassis
recipes.remove(<enderio:item_material>);
recipes.addShaped(<enderio:item_material>, 
    [[<basemetals:steel_bars>, <immersiveengineering:material:8>, <basemetals:steel_bars>],
     [<immersiveengineering:material:8>, <enderio:item_material:20>, <immersiveengineering:material:8>], 
     [<basemetals:steel_bars>, <moreplates:redstone_gear>, <basemetals:steel_bars>]]);
