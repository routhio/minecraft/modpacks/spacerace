# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading galacticraft.zs");

# Coal Generator
recipes.remove(<galacticraftcore:machine>);
recipes.addShaped(<galacticraftcore:machine>,
    [[<basemetals:copper_plate>, <basemetals:copper_plate>, <basemetals:copper_plate>],
     [<modernmetals:galvanizedsteel_plate>, <enderio:item_material:1>, <modernmetals:galvanizedsteel_plate>], 
     [<modernmetals:galvanizedsteel_plate>, <galacticraftcore:aluminum_wire>, <modernmetals:galvanizedsteel_plate>]]);

# Compressor
recipes.remove(<galacticraftcore:machine:12>);
recipes.addShaped(<galacticraftcore:machine:12>,
    [[<modernmetals:galvanizedsteel_plate>, <minecraft:anvil>, <modernmetals:galvanizedsteel_plate>],
     [<modernmetals:galvanizedsteel_plate>, <enderio:item_material:1>, <modernmetals:galvanizedsteel_plate>], 
     [<modernmetals:galvanizedsteel_plate>, <galacticraftcore:basic_item:13>, <modernmetals:galvanizedsteel_plate>]]);

# Circuit Fabricator
recipes.remove(<galacticraftcore:machine2:4>);
recipes.addShaped(<galacticraftcore:machine2:4>,
    [[<modernmetals:galvanizedsteel_plate>, <minecraft:lever>, <modernmetals:galvanizedsteel_plate>],
     [<minecraft:stone_button>, <enderio:item_material:1>, <minecraft:stone_button>],
     [<galacticraftcore:aluminum_wire>, <moreplates:redstone_gear>, <galacticraftcore:aluminum_wire>]]);

# Deconstructor
recipes.remove(<galacticraftcore:machine2:12>);
recipes.addShaped(<galacticraftcore:machine2:12>,
    [[<modernmetals:galvanizedsteel_plate>, <ore:toolShears>, <modernmetals:galvanizedsteel_plate>],
     [<galacticraftcore:aluminum_wire>, <minecraft:anvil>, <galacticraftcore:aluminum_wire>],
     [<modernmetals:galvanizedsteel_plate>, <enderio:item_material:1>, <modernmetals:galvanizedsteel_plate>]]);

# Electic Furnace
recipes.remove(<galacticraftcore:machine_tiered:4>);
recipes.addShaped(<galacticraftcore:machine_tiered:4>,
    [[<modernmetals:galvanizedsteel_plate>, <modernmetals:galvanizedsteel_plate>, <modernmetals:galvanizedsteel_plate>],
     [<modernmetals:galvanizedsteel_plate>, <enderio:item_material:1>, <modernmetals:galvanizedsteel_plate>], 
     [<galacticraftcore:basic_item:8>, <galacticraftcore:basic_item:13>, <galacticraftcore:basic_item:8>]]);

# Refinery
recipes.remove(<galacticraftcore:refinery>);
recipes.addShaped(<galacticraftcore:refinery>,
    [[null, <galacticraftcore:canister:1>, null],
     [<immersiveengineering:material:8>, <galacticraftcore:canister:1>, <immersiveengineering:material:8>], 
     [<modernmetals:galvanizedsteel_plate>, <enderio:item_material:1>, <modernmetals:galvanizedsteel_plate>]]);

# NASA Workbench
recipes.remove(<galacticraftcore:rocket_workbench>);
recipes.addShaped(<galacticraftcore:rocket_workbench>,
    [[<ore:plateStainlesssteel>, <ore:craftingTableWood>, <ore:plateStainlesssteel>],
     [<ore:plateRedstoneAlloy>, <galacticraftcore:basic_item:14>, <ore:plateRedstoneAlloy>], 
     [<ore:plateStainlesssteel>, <ore:gearRedstone>, <ore:plateStainlesssteel>]]);
