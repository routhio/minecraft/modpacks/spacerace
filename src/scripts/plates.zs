# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading plates.zs");

# Remove all plate crafting recipes.
var platesToRemove = [
    <basemetals:nickel_dense_plate>,
    <basemetals:zinc_dense_plate>,
    <basemetals:adamantine_dense_plate>,
    <basemetals:coldiron_dense_plate>,
    <basemetals:platinum_dense_plate>,
    <ic2:plate:0>,
    <ic2:plate:1>,
    <ic2:plate:2>,
    <ic2:plate:3>,
    <ic2:plate:5>,
    <ic2:plate:6>,
    <ic2:plate:7>,
    <ic2:plate:8>,
    <ic2:plate:15>,
    <immersiveengineering:metal:30>,
    <immersiveengineering:metal:31>,
    <immersiveengineering:metal:32>,
    <immersiveengineering:metal:33>,
    <immersiveengineering:metal:34>,
    <immersiveengineering:metal:35>,
    <immersiveengineering:metal:36>,
    <immersiveengineering:metal:37>,
    <immersiveengineering:metal:38>,
    <immersiveengineering:metal:39>,
    <immersiveengineering:metal:40>,
    <modernmetals:aluminum_dense_plate>,
    <modernmetals:chromium_dense_plate>,
    <modernmetals:magnesium_dense_plate>,
    <modernmetals:rutile_dense_plate>,
    <modernmetals:galvanizedsteel_dense_plate>,
    <modernmetals:nichrome_dense_plate>,
    <modernmetals:stainlesssteel_dense_plate>,
    <modernmetals:uranium_dense_plate>,
    <modernmetals:titanium_dense_plate>
] as IItemStack[];

for plate in platesToRemove {
    rh(plate);
}

var platesRecipesToRemove = [
    <basemetals:copper_plate>,
    <basemetals:lead_plate>,
    <basemetals:nickel_plate>,
    <basemetals:silver_plate>,
    <basemetals:tin_plate>,
    <basemetals:zinc_plate>,
    <basemetals:brass_plate>,
    <basemetals:bronze_plate>,
    <basemetals:electrum_plate>,
    <basemetals:invar_plate>,
    <basemetals:pewter_plate>,
    <basemetals:steel_plate>,
    <basemetals:adamantine_plate>,
    <basemetals:coldiron_plate>,
    <basemetals:platinum_plate>,
    <basemetals:iron_plate>,
    <basemetals:gold_plate>,
    <modernmetals:aluminum_plate>,
    <modernmetals:chromium_plate>,
    <modernmetals:magnesium_plate>,
    <modernmetals:rutile_plate>,
    <modernmetals:galvanizedsteel_plate>,
    <modernmetals:nichrome_plate>,
    <modernmetals:stainlesssteel_plate>,
    <modernmetals:uranium_plate>,
    <modernmetals:titanium_plate>
] as IItemStack[];

for plate in platesRecipesToRemove {
    recipes.remove(plate);
}

# Remove Casting recipes for plates
mods.tconstruct.Casting.removeTableRecipe(<basemetals:zinc_plate>);
mods.tconstruct.Casting.removeTableRecipe(<basemetals:brass_plate>);
mods.tconstruct.Casting.removeTableRecipe(<basemetals:invar_plate>);
mods.tconstruct.Casting.removeTableRecipe(<basemetals:pewter_plate>);
mods.tconstruct.Casting.removeTableRecipe(<basemetals:adamantine_plate>);
mods.tconstruct.Casting.removeTableRecipe(<basemetals:coldiron_plate>);
mods.tconstruct.Casting.removeTableRecipe(<basemetals:platinum_plate>);
mods.tconstruct.Casting.removeTableRecipe(<modernmetals:chromium_plate>);
mods.tconstruct.Casting.removeTableRecipe(<modernmetals:magnesium_plate>);
mods.tconstruct.Casting.removeTableRecipe(<modernmetals:rutile_plate>);
mods.tconstruct.Casting.removeTableRecipe(<modernmetals:galvanizedsteel_plate>);
mods.tconstruct.Casting.removeTableRecipe(<modernmetals:nichrome_plate>);
mods.tconstruct.Casting.removeTableRecipe(<modernmetals:stainlesssteel_plate>);
mods.tconstruct.Casting.removeTableRecipe(<modernmetals:titanium_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:conductive_iron_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:dark_steel_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:electrical_steel_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:end_steel_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:energetic_alloy_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:iron_alloy_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:pulsating_iron_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:redstone_alloy_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:soularium_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:vibrant_alloy_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:crude_steel_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:crystalline_alloy_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:crystalline_pink_slime_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:energetic_silver_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:melodic_alloy_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:stellar_alloy_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:vivid_alloy_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:alumite_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:ardite_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:cobalt_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:knightslime_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:manyullyn_plate>);
mods.tconstruct.Casting.removeTableRecipe(<moreplates:pig_iron_plate>);

# Plates from ingots and a hammer
recipes.addShaped(<basemetals:iron_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotIron>], [<ore:ingotIron>]]);
recipes.addShaped(<basemetals:gold_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotGold>], [<ore:ingotGold>]]);
recipes.addShaped(<basemetals:copper_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotCopper>], [<ore:ingotCopper>]]);
recipes.addShaped(<basemetals:lead_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotLead>], [<ore:ingotLead>]]);
recipes.addShaped(<basemetals:nickel_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotNickel>], [<ore:ingotNickel>]]);
recipes.addShaped(<basemetals:silver_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotSilver>], [<ore:ingotSilver>]]);
recipes.addShaped(<basemetals:tin_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotTin>], [<ore:ingotTin>]]);
recipes.addShaped(<basemetals:zinc_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotZinc>], [<ore:ingotZinc>]]);
recipes.addShaped(<basemetals:brass_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotBrass>], [<ore:ingotBrass>]]);
recipes.addShaped(<basemetals:bronze_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotBronze>], [<ore:ingotBronze>]]);
recipes.addShaped(<basemetals:electrum_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotElectrum>], [<ore:ingotElectrum>]]);
recipes.addShaped(<basemetals:invar_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotInvar>], [<ore:ingotInvar>]]);
recipes.addShaped(<basemetals:pewter_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotPewter>], [<ore:ingotPewter>]]);
recipes.addShaped(<basemetals:steel_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotSteel>], [<ore:ingotSteel>]]);
recipes.addShaped(<basemetals:adamantine_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotAdamantine>], [<ore:ingotAdamantine>]]);
recipes.addShaped(<basemetals:coldiron_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotColdiron>], [<ore:ingotColdiron>]]);
recipes.addShaped(<basemetals:platinum_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotPlatinum>], [<ore:ingotPlatinum>]]);

recipes.addShaped(<modernmetals:aluminum_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotAluminum>], [<ore:ingotAluminum>]]);
recipes.addShaped(<modernmetals:chromium_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotChromium>], [<ore:ingotChromium>]]);
recipes.addShaped(<modernmetals:magnesium_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotMagnesium>], [<ore:ingotMagnesium>]]);
recipes.addShaped(<modernmetals:rutile_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotRutile>], [<ore:ingotRutile>]]);
#recipes.addShaped(<modernmetals:aluminumbrass_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotAluminumbrass>], [<ore:ingotAluminumbrass>]]);
recipes.addShaped(<modernmetals:galvanizedsteel_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotGalvanizedsteel>], [<ore:ingotGalvanizedsteel>]]);
recipes.addShaped(<modernmetals:nichrome_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotNichrome>], [<ore:ingotNichrome>]]);
recipes.addShaped(<modernmetals:stainlesssteel_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotStainlesssteel>], [<ore:ingotStainlesssteel>]]);
recipes.addShaped(<modernmetals:uranium_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotUranium>], [<ore:ingotUranium>]]);
recipes.addShaped(<modernmetals:titanium_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotTitanium>], [<ore:ingotTitanium>]]);

recipes.addShaped(<moreplates:conductive_iron_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotConductiveIron>], [<ore:ingotConductiveIron>]]);
recipes.addShaped(<moreplates:dark_steel_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotDarkSteel>], [<ore:ingotDarkSteel>]]);
recipes.addShaped(<moreplates:electrical_steel_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotElectricalSteel>], [<ore:ingotElectricalSteel>]]);
recipes.addShaped(<moreplates:end_steel_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotEndSteel>], [<ore:ingotEndSteel>]]);
recipes.addShaped(<moreplates:energetic_alloy_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotEnergeticAlloy>], [<ore:ingotEnergeticAlloy>]]);
recipes.addShaped(<moreplates:iron_alloy_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotConstructionAlloy>], [<ore:ingotConstructionAlloy>]]);
recipes.addShaped(<moreplates:pulsating_iron_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotPulsatingIron>], [<ore:ingotPulsatingIron>]]);
recipes.addShaped(<moreplates:redstone_alloy_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotRedstoneAlloy>], [<ore:ingotRedstoneAlloy>]]);
recipes.addShaped(<moreplates:soularium_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotSoularium>], [<ore:ingotSoularium>]]);
recipes.addShaped(<moreplates:vibrant_alloy_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotVibrantAlloy>], [<ore:ingotVibrantAlloy>]]);
recipes.addShaped(<moreplates:crude_steel_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotCrudeSteel>], [<ore:ingotCrudeSteel>]]);
recipes.addShaped(<moreplates:crystalline_alloy_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotCrystallineAlloy>], [<ore:ingotCrystallineAlloy>]]);
recipes.addShaped(<moreplates:crystalline_pink_slime_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotCrystallinePinkSlime>], [<ore:ingotCrystallinePinkSlime>]]);
recipes.addShaped(<moreplates:energetic_silver_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotEnergeticSilver>], [<ore:ingotEnergeticSilver>]]);
recipes.addShaped(<moreplates:melodic_alloy_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotMelodicAlloy>], [<ore:ingotMelodicAlloy>]]);
recipes.addShaped(<moreplates:stellar_alloy_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotStellarAlloy>], [<ore:ingotStellarAlloy>]]);
recipes.addShaped(<moreplates:vivid_alloy_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotVividAlloy>], [<ore:ingotVividAlloy>]]);

recipes.addShaped(<moreplates:alumite_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotAlumite>], [<ore:ingotAlumite>]]);
recipes.addShaped(<moreplates:ardite_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotArdite>], [<ore:ingotArdite>]]);
recipes.addShaped(<moreplates:cobalt_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotCobalt>], [<ore:ingotCobalt>]]);
recipes.addShaped(<moreplates:knightslime_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotKnightslime>], [<ore:ingotKnightslime>]]);
recipes.addShaped(<moreplates:manyullyn_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotManyullyn>], [<ore:ingotManyullyn>]]);
recipes.addShaped(<moreplates:pig_iron_plate>, [[<ore:craftingToolForgeHammer>.reuse().transformDamage(4)], [<ore:ingotPigiron>], [<ore:ingotPigiron>]]);
