# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

import crafttweaker.item.IItemStack as IItemStack;
import mods.jei.JEI.removeAndHide as rh;

print(">>> loading rods.zs");

# Remove all plate crafting recipes.
# Remove all rod crafting recipes.
var rodsToRemove = [
    <microblockcbe:stone_rod>,
    <basemetals:stone_rod>,
    <moreplates:bronze_stick>,
    <moreplates:constantan_stick>,
    <moreplates:constantan_stick>,
    <moreplates:copper_stick>,
    <moreplates:electrum_stick>,
    <moreplates:invar_stick>,
    <moreplates:lead_stick>,
    <moreplates:nickel_stick>,
    <moreplates:silver_stick>,
    <moreplates:tin_stick>,
    <moreplates:gold_stick>,
] as IItemStack[];

for rod in rodsToRemove {
    rh(rod);
}

var rodRecipesToRemove = [
    <basemetals:copper_rod>,
    <basemetals:lead_rod>,
    <basemetals:nickel_rod>,
    <basemetals:silver_rod>,
    <basemetals:tin_rod>,
    <basemetals:zinc_rod>,
    <basemetals:brass_rod>,
    <basemetals:bronze_rod>,
    <basemetals:electrum_rod>,
    <basemetals:invar_rod>,
    <basemetals:pewter_rod>,
    <basemetals:steel_rod>,
    <basemetals:adamantine_rod>,
    <basemetals:coldiron_rod>,
    <basemetals:platinum_rod>,
    <basemetals:iron_rod>,
    <basemetals:gold_rod>,
    <modernmetals:aluminum_rod>,
    <modernmetals:chromium_rod>,
    <modernmetals:magnesium_rod>,
    <modernmetals:rutile_rod>,
    <modernmetals:galvanizedsteel_rod>,
    <modernmetals:nichrome_rod>,
    <modernmetals:stainlesssteel_rod>,
    <modernmetals:uranium_rod>,
    <modernmetals:titanium_rod>
] as IItemStack[];

for rod in rodRecipesToRemove {
    recipes.remove(rod);
}

