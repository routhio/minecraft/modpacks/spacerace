# vim:set ts=2 sw=2 sts=2 et nowrap ff=unix
#

print(">>> loading ic2.zs");

# Universal Fluid Cell
recipes.remove(<ic2:fluid_cell>);
recipes.addShaped(<ic2:fluid_cell> * 4, 
                  [[<ore:craftingToolForgeHammer>.reuse().transformDamage(5), <ore:plateTin>, null],
                   [<ore:plateTin>, <ore:paneGlassColorless>, <ore:plateTin>],
                   [null, <ore:plateTin>, null]]);
