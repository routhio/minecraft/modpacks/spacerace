FROM registry.routh.io/minecraft/docker/server:0.3.7

RUN mkdir -p /dist/spacerace
ADD packmaker.yml   /dist/spacerace/packmaker.yml
ADD packmaker.lock  /dist/spacerace/packmaker.lock
COPY src            /dist/spacerace/src

ENV ENTRY_CMD="mine-init start -c /cache -d /dist/spacerace/"
